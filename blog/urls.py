from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'blog.views.index', name='home'),
    url(r'^view_posts/(?P<page_num>[0-9]*)/$', 'blog.views.view_posts', name='view_posts'),
    url(r'^view_post/(?P<post_id>[0-9]*)/$', 'blog.views.view_post', name='view_post'),
    url(r'^create_post/$', 'blog.views.create_post', name='create_post'),
    url(r'^delete_post/(?P<post_id>[0-9]*)/$', 'blog.views.delete_post', name='delete_post'),
    url(r'^register/$', 'blog.views.register', name='register'),
    url(r'^login/$', 'blog.views.login', name='login'),
    url(r'^logout/$', 'blog.views.logout', name='logout'),

    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)