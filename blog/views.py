from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseServerError
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from blog.models import *
from blog.forms import *


def index(request):
    """
    Return a static page as the homepage
    """
    return render_to_response("index.html", {}, context_instance=RequestContext(request))


def view_posts(request, page_num):
    """
    Display up to 2 posts, along with the post comments
    """
    posts = Post.objects.all()
    paginator = Paginator(posts, 2)
    max_page = paginator.num_pages
    try:
        page_num = int(page_num)
    except ValueError:
        pass
    try:
        posts = paginator.page(page_num)
    except (InvalidPage, EmptyPage):
        page_num = 1
        posts = paginator.page(max_page)
    return render_to_response("view_posts.html", locals(), context_instance=RequestContext(request))


def register(request):
    """
    Register a user, so that they can make posts to the site
    """
    next_page = reverse('view_posts', kwargs={'page_num': '1'})
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = User.objects.create_user(username=cd['username'], password=cd['password'])
            user.first_name = cd['first_name']
            user.last_name = cd['last_name']
            user.email = cd['email']
            user.save()
            messages.info(request, 'You have successfully registered, please sign in')
            return HttpResponseRedirect(next_page)
    else:
        form = RegistrationForm()
    return render_to_response("register.html", locals(), context_instance=RequestContext(request))


def login(request):
    """
    Log the user onto the site
    """
    next_page = reverse('view_posts', kwargs={'page_num': '1'})
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = auth.authenticate(username=cd['username'], password=cd['password'])
            if not user:
                # If no user is found, then the form failed to validate the account - return a 500 error
                return HttpResponseServerError()
            else:
                auth.login(request, user)
                messages.info(request, 'Welcome, %s' % user.first_name)
                return HttpResponseRedirect(next_page)
    else:
        form = LoginForm()
    return render_to_response("login.html", locals(), context_instance=RequestContext(request))


def logout(request):
    next_page = request.META.get('HTTP_REFERER', None) or reverse('view_posts', kwargs={'page_num': '1'})
    auth.logout(request)
    messages.info(request, 'You have logged out')
    return HttpResponseRedirect(next_page)


@login_required(login_url="/login/")
def create_post(request):
    next_page = reverse('view_posts', kwargs={'page_num': '1'})
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            title = cd["title"]
            body = cd['body']
            post = Post(title=title, body=body, created_by=request.user)
            post.save()
            messages.info(request, 'Post: \'%s\' has been submitted' % title)
            return HttpResponseRedirect(next_page)
    else:
        form = PostForm()
    return render_to_response("create_post.html", locals(), context_instance=RequestContext(request))


@login_required(login_url="/login/")
def delete_post(request, post_id):
    """
    Ensure that the post is owned by the logged in user, and if so delete it
    """
    next_page = reverse('view_posts', kwargs={'page_num': '1'})
    post = get_object_or_404(Post, id=int(post_id))
    title = post.title
    if post.created_by == request.user:
        post.delete()
        message = 'Post: \'%s\' has been deleted' % title
    else:
        message = 'You cannot delete Post: \'%s\' as this was not created by you' % title
    if request.is_ajax():
        return HttpResponse(message)
    else:
        messages.info(request, message)
        return HttpResponseRedirect(next_page)


def view_post(request, post_id):
    """
    View a single post
    """
    post = get_object_or_404(Post, id=int(post_id))
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            created_by = cd["name"]
            body = cd['body']
            comment = Comment(post=post, created_by=created_by, body=body)
            comment.save()
            messages.info(request, 'Your comment has been added')
    form = CommentForm()
    return render_to_response("view_post.html", locals(), context_instance=RequestContext(request))