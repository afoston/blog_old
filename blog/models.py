from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=60)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.OneToOneField(User)

    def __unicode__(self):
        return u'%s created on %s' % (self.title, self.created)

    class Meta():
        ordering = ['-created']


class Comment(models.Model):
    post = models.ForeignKey(Post)
    body = models.TextField()
    created_by = models.CharField(max_length=60)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s commented on \'%s\' post on %s' % (self.user_name, self.post.title, self.created)

    class Meta():
        ordering = ['created']