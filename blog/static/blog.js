


$(document).ready(function(){
    $("#show_comments").click(function(){
        var link = $("#show_comments");
        var div = $("#comments");
        if (div.is(":visible")){
            link.text("Show Comments");
            div.hide();
        } else {
            link.text("Hide Comments");
            div.show();
        }
        return false;
    });

    $("#delete_post").click(function(){
        var messages = $('#messages');
        $( "#dialog-confirm" ).dialog({
            resizable: false,
            height:170,
            modal: true,
            buttons: {
                "OK": function() {
                    var url = $("#delete_post").attr("href");
                    messages.load(url, function(response, status, xhr){
                        if (status == "error"){
                            messages.html('An error has occurred whilst trying to delete this post');
                        }
                        messages.show().delay(5000).hide(400);
                        window.location.href = "/";
                    });
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        return false;
    });
});

// ----Content from here onwards can probably be deleted

// Date and Time jQuery UI for rules forms
$(function() {
   $( ".rules_form #id_date" ).datepicker({ dateFormat: "dd/mm/yy" });
   $( ".rules_form #id_start" ).timepicker({stepMinute: 5});
   $( ".rules_form #id_end" ).timepicker({stepMinute: 5});
});



$(document).ready(function(){
   $("#check_call a").click(function(){
      var link = $("#check_call a");
      link.attr('disabled','disabled');
      $('#messages').html('')
      $('#messages').load('/duty/check_call/', function(response, status, xhr){
         if (status == "error"){
            $('#messages').html('An error has occurred');
         }
         $('#messages').show().delay(5000).hide(400);
         link.removeAttr('disabled');
      })
      return false;
   });
   $("#book_off a").click(function(){
      var link = $("#book_off a");
      link.attr('disabled','disabled');
      $('#messages').html('')
      $('#messages').load('/duty/book_off_duty/', function(response, status, xhr){
         if (status == "error"){
            $('#messages').html('An error has occurred');
         }
         $('#messages').show().delay(5000).hide(400);
         $('#book_off').hide();
         $('#check_call').hide();
         $('#book_on').show();
      })
      return false;
   });
   $("#elements").on("click", ".hide_alert", function(){
      var button = $(this);
      var alertId = button.attr('id');
      $( "#dialog-confirm" ).dialog({
         resizable: false,
         height:170,
         modal: true,
         buttons: {
           "OK": function() {
              $("#messages").html('');
              $("#messages").load('/accounts/hide_alert/' + alertId + '/', function(){
                 $("#elements").load('/');
                 $('#messages').show().delay(5000).hide(400);
              });
              $( this ).dialog( "close" );
            },
            Cancel: function() {
               $( this ).dialog( "close" );
            }
         }
      });
   });
});



