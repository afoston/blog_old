from django import forms
from django.contrib.auth.models import User
from django.contrib import auth


class RegistrationForm(forms.Form):
    first_name = forms.CharField(min_length=3)
    last_name = forms.CharField(min_length=3)
    username = forms.CharField(min_length=5)
    password = forms.CharField(widget=forms.PasswordInput(render_value=False), label='Password', min_length=5)
    confirm_password = forms.CharField(widget=forms.PasswordInput(render_value=False),
                                       label='Confirm Password', min_length=5)
    email = forms.EmailField()

    def clean_password(self):
        import re
        password = self.cleaned_data['password']
        if re.search('[A-Z]', password):
            if re.search('[a-z]', password):
                if re.search('[0-9]', password):
                    if re.search('[^A-Za-z0-9]', password):
                        #Contains uppercase, lowercase, digits and symbols
                        return password
        raise forms.ValidationError('Password must contain uppercase characters, lowercase characters,'
                                    ' numbers and symbols')

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).count() == 0:
            return username
        else:
            raise forms.ValidationError('The username %s already exists' % username)

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        if password != confirm_password:
            raise forms.ValidationError('The password and the confirmation password must match')
        return cleaned_data


class LoginForm(forms.Form):
    username = forms.CharField(min_length=5)
    password = forms.CharField(widget=forms.PasswordInput(render_value=False), label='Password', min_length=5)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        user = auth.authenticate(username=username, password=password)
        if user is None:
            raise forms.ValidationError('Invalid Username/Password')
        return cleaned_data

class PostForm(forms.Form):
    title = forms.CharField(max_length=60, min_length=1)
    body = forms.CharField(widget=forms.Textarea, min_length=1)


class CommentForm(forms.Form):
    body = forms.CharField(widget=forms.Textarea, min_length=1)
    name = forms.CharField(max_length=60, min_length=1)
